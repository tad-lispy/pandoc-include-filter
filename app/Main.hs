module Main where

import qualified Text.Pandoc.Filter.Include    as Include
import           Text.Pandoc.JSON               ( toJSONFilter )


main :: IO ()
main = toJSONFilter Include.processBlock

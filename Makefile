.PHONY: all
all: build


.PHONY: build
build:
	stack build

.PHONY: test
test:
	stack test

.PHONY: install
install: build test
	stack install

README.md: content/README.md
	pandoc \
		--output $@ \
		--filter pandoc-include-filter \
		--to gfm \
		$^

content/output.md: content/document.md
	pandoc \
		--output $@ \
		--filter pandoc-include-filter \
		--to gfm \
		$^

# Changelog for pandoc-include-filter

## Unreleased changes

### Added

  - Initial release
  - Support for include attribute
  - Support for startFrom attribute
  - Support for endAt attribute

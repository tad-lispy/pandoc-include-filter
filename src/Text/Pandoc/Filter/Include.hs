module Text.Pandoc.Filter.Include
  ( processBlock
  )
where

import           Text.Pandoc.JSON
import           Data.Maybe

processBlock :: Block -> IO Block
processBlock cb@(CodeBlock (id, classes, namevals) _) =
  case lookup "include" namevals of
    Nothing -> return cb
    Just path ->
      let fromLine   = maybe 1 read $ lookup "startFrom" namevals
          toLine     = maybe maxBound read $ lookup "endAt" namevals
          linesCount = toLine - fromLine + 1
      in  do
            contents <- readFile path
            return
              $ CodeBlock (id, classes, namevals)
              $ unlines
              $ take linesCount
              $ drop (fromLine - 1)
              $ lines contents
processBlock x = return x

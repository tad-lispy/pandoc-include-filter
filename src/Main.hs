module Main where

import           Text.Pandoc.JSON
import           Data.Maybe

doInclude :: Block -> IO Block
doInclude cb@(CodeBlock (id, classes, namevals) _) =
  case lookup "include" namevals of
    Nothing -> return cb
    Just path ->
      let fromLine   = maybe 1 read $ lookup "startFrom" namevals
          toLine     = maybe maxBound read $ lookup "endAt" namevals
          linesCount = toLine - fromLine + 1
      in  do
            contents <- readFile path
            return
              $ CodeBlock (id, classes, namevals)
              $ unlines
              $ take linesCount
              $ drop (fromLine - 1)
              $ lines contents
doInclude x = return x

main :: IO ()
main = toJSONFilter doInclude

# A Document with Some Source Code Samples

Here is a complete source code of `Main.elm`:

``` elm
module Main exposing (main)

main =
    Html.text "Hello, Pandoc filters!"
```

Just look at this pearl of programming\!

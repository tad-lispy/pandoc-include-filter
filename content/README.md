# pandoc-include-filter

A filter for [Pandoc][] that can read files and substitute code blocks with their contents.

## Use

Given a markdown file located at `content/document.md` with the following content:

``` { include="content/document.md" .markdown }
contents of markdown file go here
```

and and a `content/Main.elm` file like this:

``` { include="content/Main.elm" .elm }
source code goes here
```

the following command:

``` { .bash }
pandoc \
  --output document.md \
  --filter pandoc-include-filter \
  --to gfm
  content/document.md
```

Will produce a `document.md` with the following contents:

``` { include="content/output.md" .markdown }
output goes here
```

You can just as well output to any other format: `pdf`, `html` etc.

## What is it good for?

I want this filter so I can generate `README` files in my projects with code samples taken from actual source code - so it's easier to keep them up to date with the evolving code.

Yes, this README is generated using this filter. Checkout `content/README.md`!

## Options


Options specific to this filter are:

  - `startFrom`: start reading from a given line

    Line numbers start at 1 (not 0) like in most text editors. The starting line is included.

  - `endAt`: finish reading at a line

    Again, lines are counted from 1 and the last line is included.

If you want only line 14 of the source file, you can do it like this:

````` { .markdown }
``` { include="some-file.c" startFrom=14 endAt=14 }
```
`````

The attributes (id, classes, options) are passed through to the
resulting file, so you can use all the standard tricks of pandoc like
syntax highlighting or line numbering. Consider:

````` { .markdown }
``` { include="some-file.c" startFrom=14 endAt=14 .numberLines .c }
```
`````

## TODO

  - File paths relative to including document

    Currently the paths are relative to the working directory where `pandoc` is executed. This is not very portable.

  - Defining ranges by code blocks

    It would be nice to be able to say something like "include a definition of `update` function" and have the filter find the lines range.


[Pandoc]: https://pandoc.org/
